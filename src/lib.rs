use subslice::SubsliceExt;

// FIXME: Should be a per-message option
const STRICT_CRLF: bool = false;

pub struct Message<'a> {
    raw: &'a [u8],
}

impl<'a> Message<'a> {
    pub fn new(data: &'a [u8]) -> Message<'a> {
        Message {
            raw: data,
        }
    }

    pub fn body(&self) -> Option<&[u8]> {
        if let Some(offset) = self.raw.find(b"\r\n\r\n") {
            return Some(&self.raw[offset+4..]);
        }

        if !STRICT_CRLF {
            if let Some(offset) = self.raw.find(b"\n\r\n") {
                return Some(&self.raw[offset+3..]);
            } else if let Some(offset) = self.raw.find(b"\n\n") {
                return Some(&self.raw[offset+2..]);
            }
        }

        None
    }
}

#[cfg(test)]
mod tests {
    use crate::Message;

    const MSG1: &[u8] =
        b"From: Alice <alice@example.com>\r
To: Bob <alice@example.com>\r
\r
body";

    #[test]
    fn trivial() {
        let _ = Message::new(MSG1);
    }

    #[test]
    fn body1() {
        let m = Message::new(MSG1);

        assert_eq!(m.body().unwrap(), b"body");
    }

    #[test]
    fn no_body() {
        let m = Message::new(b"");

        assert_eq!(m.body(), None);
    }

    #[test]
    fn empty_body() {
        let m  = Message::new(b"\r\n\r\n");

        assert_eq!(m.body().unwrap(), b"");
    }
}
