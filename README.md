laz-e-mail - Lazy parsing of RFC822/2822/5322 email messages
===

This is a library for parsing email messages in the format defined by
RFC822 (later revised by RFC2822 and RFC5322).  It's distinguished
from other such libraries by these design goals:

 * Be lazy.  Don't immediately fully parse the message into an
   internal structure - only parse what we need to retrieve specific
   information from the message.  Just reading messages into the
   system and then sending them out again should do minimal work.

 * But cache.  Once the user does request data that needs parsing,
   cache the results in suitable data structures so that if lots more
   lookups are done they're then fast.

In addition we try to:
 * Cope with ill-formatted messages.  Even if the input is not RFC822
   compliant, try to return as much useful data as possible.

These are the same design principles as the [`rfc822`][1] module in
ccan.

## License

This library is licensed under the LGPL, version 2.1 or later.

[1]: https://ccodearchive.net/info/rfc822.html
